package com.cheese.game.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.cheese.game.FirstCheesePlease;

public class DesktopLauncher {
	public static void main (String[] arg) {
		/*LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		new LwjglApplication(new FirstCheesePlease(), config);*/
		CheesePlease1 myProgram = new CheesePlease1();
		LwjglApplication launcher = new LwjglApplication( myProgram );
	}
}
